from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

import time

from bs4 import BeautifulSoup


class ParserService:
    __urls: list[str] = [
        'https://moskva.mts.ru/personal/mobilnaya-svyaz/tarifi/vse-tarifi/mobile-tv-inet',
        'https://moskva.mts.ru/personal/mobilnaya-svyaz/tarifi/vse-tarifi/mobile',
        'https://moskva.mts.ru/personal/mobilnaya-svyaz/tarifi/vse-tarifi/watches-and-modems',
        'https://moskva.mts.ru/personal/mobilnaya-svyaz/tarifi/vse-tarifi/telefon',
    ]

    def __init__(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--headless')
        self.driver: webdriver = webdriver.Chrome(options=self.options)

    def parse_rates(self):
        '''Распарсить тарифы из страниц и сгргруппировать данные для загрузки в бд'''
        pages: list = self._get_pages()
        rates: list[dict] = []

        for page in pages:
            soup = BeautifulSoup(page, 'html.parser')
            category = soup.find("div", attrs={"class": "card card_size card_selected"})

            for card in soup.find_all('mts-tariff-card'):
                name = card.find('span', attrs={'class': 'card-title__link'})
                descriptions = card.find('div', attrs={'class': 'card-description card-description__margin'})
                price = card.find('span', attrs={'class': 'price-text'})

                if name and descriptions and price and category:
                    rates.append(
                        {
                            'name': name.text,
                            'descriptions': descriptions.text.replace('\xa0', ' '),
                            'price': price.text,
                            'category': category.text
                        }
                    )
        print(rates)

        return rates

    def _get_pages(self):
        '''Получить список со страницами тарифов разных категорий'''
        pages: list = []
        for url in self.__urls:
            self.driver.get(url)
            time.sleep(2)
            pages.append(self._get_source(self.driver))

        print(len(pages))
        return pages

    def _get_source(self, driver: webdriver):
        '''Метод возвращающий страницу с тарифами определенной категории'''
        time.sleep(2)
        try:
            more_button = driver.find_element(By.XPATH, '//button[@class="btn btn_secondary"]')
            time.sleep(3)
            more_button.click()

            return driver.page_source

        except NoSuchElementException:
            return driver.page_source


if __name__ == '__main__':
    parser = ParserService().parse_rates()
